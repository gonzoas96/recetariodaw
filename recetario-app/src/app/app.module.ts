import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ComponentsModule } from './shared/components.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthLayoutComponent } from './pages/auth-layout/auth-layout.component';
import { routes } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { HomeLayoutComponent } from './pages/home-layout/home-layout.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    HomeLayoutComponent
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule.forRoot(routes),
    MDBBootstrapModule.forRoot()

  ],
  providers: [{provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true},],
  bootstrap: [AppComponent]
})
export class AppModule { }
