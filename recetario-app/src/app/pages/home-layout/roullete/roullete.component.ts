import { Component, OnInit } from '@angular/core';
import { UpdateService } from 'src/app/services/update.service';
import { Recipe2 } from 'src/app/shared/models/recipe2.model';

@Component({
  selector: 'app-roullete',
  templateUrl: './roullete.component.html',
  styleUrls: ['./roullete.component.scss']
})
export class RoulleteComponent implements OnInit {
  isLoading = false;
  error = false;
  recipes : Recipe2[] = [new Recipe2()];
  constructor( private updateService : UpdateService) { }

  ngOnInit(): void {

  }

  getRandBreakfast(){
    this.isLoading = true;
    setTimeout(() =>
    {
      this.updateService.getRandomRecipe("Breakfast").subscribe(
        resData=>{
          this.recipes = resData.recipe;
          console.log(this.recipes[0]._id);
          this.isLoading = false;
      }, errorMessage=>{
          this.isLoading = false;
          this.error = true;
          console.log(errorMessage);
          // this.error=errorMessage;
      });
    },
    1000);
  }


  getRandLunch(){
    this.isLoading = true;
    setTimeout(() =>
    {
      this.updateService.getRandomRecipe("Lunch").subscribe(
        resData=>{
          this.recipes = resData.recipe;
          console.log(this.recipes);
          this.isLoading = false;
      }, errorMessage=>{
          this.isLoading = false;
          this.error = true;
          console.log(errorMessage);
          // this.error=errorMessage;
      });
    },
    1000);
  }


  getRandDinner(){
    this.isLoading = true;
    setTimeout(() =>
    {
      this.updateService.getRandomRecipe("Dinner").subscribe(
        resData=>{
          this.recipes = resData.recipe;
          console.log(this.recipes);
          this.isLoading = false;
      }, errorMessage=>{
          this.isLoading = false;
          this.error = true;
          console.log(errorMessage);
          // this.error=errorMessage;
      });
    },
    1000);
  }
}
