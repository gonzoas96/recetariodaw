import { Component, DoCheck, OnInit } from '@angular/core';
import { UpdateService } from 'src/app/services/update.service';
import { Recipe } from 'src/app/shared/models/recipe.model';
import Swal from 'node_modules/sweetalert2/dist/sweetalert2.all.js';

@Component({
  selector: 'app-my-recipies',
  templateUrl: './my-recipies.component.html',
  styleUrls: ['./my-recipies.component.css']
})
export class MyRecipiesComponent implements OnInit {
  recipies = [];
  isLoading : boolean = false;
  error : boolean = false;
  constructor(private updateService : UpdateService) { }

  ngOnInit() {
    this.updateService.getMyRecipies().subscribe(
      resData=>{
        this.isLoading = false;
        console.log(resData);
        this.recipies = resData.recipes;
    }, errorMessage=>{
        this.isLoading = false;
        this.error = true;
        console.log(errorMessage);
        // this.error=errorMessage;
    });
  }


  onDeleteRecipe(id: string, index: number){
    let recipe_id = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.updateService.deleteRecipe(recipe_id).subscribe( resData=>{
          Swal.fire(
            'Deleted!',
            'Your Recipe has been deleted.',
            'success'
          )
          this.recipies.splice(index,1);
        }, errorMessage=>{
          Swal.fire(
            'Error!',
            'Your Recipe has not been deleted.',
            'success'
          )
        });
      }
    })
  }


}
