import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components.module';
import { HomeLayoutRoutes } from './home-layout.routing';
import { RoulleteComponent } from './roullete/roullete.component';
import { RecipeCatalogComponent } from './recipe-catalog/recipe-catalog.component';
import { MyRecipiesComponent } from './my-recipies/my-recipies.component';
import { OneRecipeComponent } from './one-recipe/one-recipe.component';
import { CreateRecipeComponent } from './create-recipe/create-recipe.component';
import { ModifyRecipeComponent } from './modify-recipe/modify-recipe.component';



@NgModule({
  imports:[
    CommonModule,
    RouterModule.forChild(HomeLayoutRoutes),
    FormsModule,
    ComponentsModule

  ],
  declarations:[
    RoulleteComponent,
    RecipeCatalogComponent,
    MyRecipiesComponent,
    OneRecipeComponent,
    CreateRecipeComponent,
    ModifyRecipeComponent
  ]
})

export class HomeLayoutModule {}
