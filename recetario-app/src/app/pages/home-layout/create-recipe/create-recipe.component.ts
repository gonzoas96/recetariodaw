import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UpdateService } from 'src/app/services/update.service';
import { Ingridient } from 'src/app/shared/models/ingridient.model';
import { Recipe } from 'src/app/shared/models/recipe.model';
import Swal from 'node_modules/sweetalert2/dist/sweetalert2.all.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.css']
})
export class CreateRecipeComponent implements OnInit{

  isLoading: boolean = false;
  @ViewChild('nameIngridient') nIngridient: ElementRef;
  @ViewChild('amountIngridient') aIngridient: ElementRef;
  ingridientsList : Ingridient[] = [];
  imageUrl : string = "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636";

  constructor(private updateService: UpdateService, private router : Router) { }

  ngOnInit(): void {
  }

  addIngridient(){
    var nameIngrid = this.nIngridient.nativeElement.value.toUpperCase();
    const newIng = new Ingridient(nameIngrid, this.aIngridient.nativeElement.value);
    this.ingridientsList.push(newIng);
    this.nIngridient.nativeElement.value ="";
    this.aIngridient.nativeElement.value ="";
  }

  onDelete(index : number){
    this.ingridientsList.splice(index,1);
  }

  onSubmit(form : NgForm){
    if(!form.valid){
      return;
    }
    const recipe = new Recipe(
      form.value.title,
      this.imageUrl,
      form.value.category,
      this.ingridientsList,
      form.value.instructions,
      form.value.description,
      form.value.meal
      );
    console.log(recipe);
    this.updateService.createNewRecipe(recipe).subscribe(
      resData=>{
        this.isLoading = false;
        console.log(resData);
        Swal.fire(
          'DONE!',
          'Your Recipe has been created',
          'success'
        )
        this.router.navigate(["/home/myRecipies"]);
        form.reset();
        this.ingridientsList = [];
        this.imageUrl =  "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636";
    }, errorMessage=>{
        this.isLoading = false;
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        })
        console.log(errorMessage);
        // this.error=errorMessage;
    });
  }
}
