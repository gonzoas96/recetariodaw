import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from '@angular/router';



@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css']
})

export class HomeLayoutComponent implements OnInit, OnDestroy {
  test: Date = new Date();
  public isCollapsed = true;

  constructor(private router: Router) {}


  ngOnInit() {

  }
  ngOnDestroy() {

  }

}
