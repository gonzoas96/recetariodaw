import {Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UpdateService } from 'src/app/services/update.service';


@Component({
  selector: 'app-one-recipe',
  templateUrl: './one-recipe.component.html',
  styleUrls: ['./one-recipe.component.css']
})
export class OneRecipeComponent {

  recipe : any = {};
  showIngridients = true;
  param1 : string;
  isLoading : boolean = false;
  error : boolean = false;
  constructor(private route: ActivatedRoute, private updateService : UpdateService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.param1 = params['recipeId'];
      this.updateService.getOneRecipe(this.param1).subscribe(
        resData=>{
          this.isLoading = false;
          console.log(resData);
          this.recipe = resData.recipe;
      }, errorMessage=>{
          this.isLoading = false;
          this.error = true;
          console.log(errorMessage);
          // this.error=errorMessage;
      });
    });

  }

  showIng(){
    this.showIngridients = true;
  }

  showInst(){
    this.showIngridients = false;
  }




}



