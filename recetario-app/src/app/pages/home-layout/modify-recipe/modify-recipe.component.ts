import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UpdateService } from 'src/app/services/update.service';
import { Ingridient } from 'src/app/shared/models/ingridient.model';
import { Recipe2 } from 'src/app/shared/models/recipe2.model';
import Swal from 'node_modules/sweetalert2/dist/sweetalert2.all.js';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-modify-recipe',
  templateUrl: './modify-recipe.component.html',
  styleUrls: ['./modify-recipe.component.css']
})
export class ModifyRecipeComponent implements OnInit {

  isLoading: boolean = false;
  @ViewChild('nameIngridient') nIngridient: ElementRef;
  @ViewChild('amountIngridient') aIngridient: ElementRef;
  recipe : any = new Recipe2();
  error : boolean = false;

  constructor(private route: ActivatedRoute,private updateService: UpdateService, private router : Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let param1 = params['recipeId'];
      this.updateService.getOneRecipe(param1).subscribe(
        resData=>{
          this.isLoading = false;
          console.log(resData);
          this.recipe = resData.recipe;
      }, errorMessage=>{
          this.isLoading = false;
          this.error = true;
          console.log(errorMessage);
      });
    });
  }

  addIngridient(){
    var nameIngrid = this.nIngridient.nativeElement.value.toUpperCase();
    const newIng = new Ingridient(nameIngrid, this.aIngridient.nativeElement.value);
    this.recipe.ingridients.push(newIng);
    this.nIngridient.nativeElement.value ="";
    this.aIngridient.nativeElement.value ="";
  }

  onDelete(index : number){
    this.recipe.ingridients.splice(index,1);
  }

  onSubmit(form : NgForm){
    if(!form.valid){
      return;
    }
    const recipe = {
      title: this.recipe.title,
      imagePath: this.recipe.imagePath,
      category: this.recipe.category,
      ingridients: this.recipe.ingridients,
      instructions: this.recipe.instructions,
      description: this.recipe.description,
      meal: this.recipe.meal,
      _id: this.recipe._id
    }
    console.log(this.recipe);
    this.updateService.modifyRecipe(this.recipe._id,recipe).subscribe(
      resData=>{
        this.isLoading = false;
        console.log(resData);
        Swal.fire(
          'DONE!',
          'Your Recipe has been modified',
          'success'
        )
        this.router.navigate(["/home/myRecipies"]);
    }, errorMessage=>{
        this.isLoading = false;
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        })
        console.log(errorMessage);
        // this.error=errorMessage;
    });
  }

}
