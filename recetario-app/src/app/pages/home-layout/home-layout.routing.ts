import { Routes } from '@angular/router';
import { CreateRecipeComponent } from './create-recipe/create-recipe.component';
import { ModifyRecipeComponent } from './modify-recipe/modify-recipe.component';
import { MyRecipiesComponent } from './my-recipies/my-recipies.component';
import { OneRecipeComponent } from './one-recipe/one-recipe.component';
import { RecipeCatalogComponent } from './recipe-catalog/recipe-catalog.component';
import { RoulleteComponent } from './roullete/roullete.component';



export const HomeLayoutRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'roullete'
  },
  {
    path: 'roullete',
    component: RoulleteComponent
  },
  {
    path: 'recipeCatalog',
    component: RecipeCatalogComponent
  },
  {
    path: 'myRecipies',
    component: MyRecipiesComponent
  },
  {
    path: 'oneRecipe/:recipeId',
    component: OneRecipeComponent
  },
  {
    path: 'createRecipe',
    component: CreateRecipeComponent
  },
  {
    path: 'modifyRecipe/:recipeId',
    component: ModifyRecipeComponent
  }
];
