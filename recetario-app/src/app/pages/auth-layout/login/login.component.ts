import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { AuthService, AuthTokenResponseData } from 'src/app/services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error:string = null;
  mode:string;
  isLoading = false;
  constructor(private authService: AuthService, private router: Router){
    this.mode = 'login';
  }


  ngOnInit() {
    if(this.authService.user.value){
      this.authService.setLoggedIn(true);
      this.router.navigate(['/home']);
    }
  }


  onSubmit(form: NgForm){
    if(!form.valid){
      return;
    }
    this.isLoading = true;
    const email=form.value.email;
    const password = form.value.password;

    let authObs: Observable<AuthTokenResponseData>

    authObs = this.authService.loginUser(email, password)

    authObs.subscribe(
      resData=>{
        this.isLoading = false;
        this.authService.setLoggedIn(true);
        // console.log("logged in!")
        this.router.navigate(['/home'])
    }, errorMessage=>{
        this.isLoading = false;
        this.error=errorMessage;
    });
    form.reset();
  }

}
