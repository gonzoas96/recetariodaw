import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService, signUpRespose } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  error:string = null;
  mode:string;
  isLoading = false;

  constructor(private authService: AuthService, private router: Router){
    this.mode = 'login';
  }
  // constructor( private router: Router){
  //   this.mode = 'login';
  // }
  ngOnInit() {
    if(this.authService.user.value){
      this.authService.setLoggedIn(true);
      this.router.navigate(['/home'])
    }
  }


  onSubmit(form: NgForm){
    if(!form.valid){
      return;
    }
    console.log("submit");
    this.isLoading = true;
    const email=form.value.email;
    const password = form.value.password;

    let authObs: Observable<signUpRespose>

    authObs = this.authService.registerUser(email, password)

    authObs.subscribe(
      resData=>{
        this.isLoading = false;
        // console.log(resData);
        // this.authService.setLoggedIn(true);
        this.router.navigate(['/login'])
    }, errorMessage=>{
        this.isLoading = false;
        this.error=errorMessage;
    });

    form.reset();
  }

  toggleMode(){
    this.mode = 'reset';
  }

}
