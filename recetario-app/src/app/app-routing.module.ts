import { Routes } from '@angular/router';
import { AuthLayoutComponent } from './pages/auth-layout/auth-layout.component';
import { HomeLayoutComponent } from './pages/home-layout/home-layout.component';


import { AuthGuard } from './services/auth.guard';


export const routes: Routes = [
  {
    path: 'home',
    component: HomeLayoutComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/home-layout/home-layout.module').then(m => m.HomeLayoutModule)
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/auth-layout/auth-layout.module').then(m => m.AuthLayoutModule)
      }
    ]
  },
  {
    path:"**", redirectTo:''
  }
];
