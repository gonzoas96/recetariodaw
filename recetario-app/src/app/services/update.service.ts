import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Recipe } from '../shared/models/recipe.model';



@Injectable({
  providedIn: 'root'
})
export class UpdateService {
  localHost = "http://localhost:3000/";

  constructor(private http: HttpClient) {

  }
  createNewRecipe(recipe: Recipe){
    return this.http.post<{message: string,recipeId:any}>(
      this.localHost + "api/recipe/",
      recipe
    )
  }

  getMyRecipies(){
    return this.http.get<{ message:string, recipes: any}>(
      this.localHost + "api/recipe/myRecipes"
    )
  }

  getOneRecipe( id : string ){
    return this.http.get<{message:string, recipe:Recipe}>(
      this.localHost + "api/recipe/"+id
    )
  }

  deleteRecipe(id : string){
    return this.http.delete<{message:string}>(
      this.localHost + "api/recipe/"+id
    )
  }

  getAllRecipies(){
    return this.http.get<{ message:string, recipes: any}>(
      this.localHost + "api/recipe/"
    )
  }


  getCategoryRecipes(category : string){
    return this.http.get<{ message:string, recipes: any}>(
      this.localHost + "api/recipe/category/"+category
    )
  }

  getRandomRecipe(meal : string){
    return this.http.get<{ message:string, recipe: any}>(
      this.localHost + "api/recipe/random/"+meal
    )
  }

  modifyRecipe(id:number, recipe:any){
    return this.http.put(
      this.localHost + "api/recipe/"+id,
      {
        "recipe":recipe
      }
    )
  }
  // getStationsOnly(){
  //   return this.http.get<any>(
  //     "https://backend-production-dot-onoff-production.uc.r.appspot.com/api/stations/"
  //   )
  // }

  // getAlgorithmStatus(){
  //   return this.http.get<any>(
  //     "https://backend-production-dot-onoff-production.uc.r.appspot.com/api/static/"
  //   )
  // }

}
