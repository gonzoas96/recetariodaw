import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { map, tap, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}




  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Promise<boolean> | boolean | Observable<boolean> {
  //   if(!this.authService.isLoggedIn){
  //     this.router.navigate(['/login']);
  //   }
  //   return this.authService.isLoggedIn;
  // }



  canActivate(
    route:ActivatedRouteSnapshot,
    router:RouterStateSnapshot
    ):
      | boolean
      | UrlTree
      | Promise<boolean | UrlTree>
      | Observable<boolean | UrlTree> {
      return this.authService.user.pipe(
        take(1),
        map(user=>{
          // console.log(user);
          const isAuth = !!user;
          if(isAuth){
            return true;
          }
          return this.router.createUrlTree(['/login']);
      }),
      );
    }

}
