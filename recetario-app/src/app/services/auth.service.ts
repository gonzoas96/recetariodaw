import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { Router } from '@angular/router';
import { User } from '../shared/models/user.model';

export interface AuthTokenResponseData {
  token: string;
}
export interface signUpRespose {
  message: string;
  result: any;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new BehaviorSubject<User>(null);
  localHost = "http://localhost:3000/";
  productionUrl = "http://noderecetariodaw-env.eba-7adazubt.us-east-2.elasticbeanstalk.com/";

  private tokenExpirationTimer: any;
  private loggedInStatus=false;

  constructor(private http: HttpClient, private router: Router){}

  setLoggedIn(value: boolean){
    this.loggedInStatus=value;
  }

  get isLoggedIn(){
    return this.loggedInStatus;
  }
//http://noderecetariodaw-env.eba-7adazubt.us-east-2.elasticbeanstalk.com/
  loginUser(email:string, password:string){
    return this.http.post<AuthTokenResponseData>(
       this.localHost + 'api/user/login',
      {
        email: email,
        password: password
      }
    ).pipe(catchError(this.handleError),
      tap(resData=>{
        // console.log(email, resData.token);
        this.handleAuthentication(email, resData.token)
      })
    );
  }

  registerUser(email:string, password:string){
    return this.http.post<signUpRespose>(
      this.localHost + 'api/user/signup',
      {
        email: email,
        password: password
      }
    ).pipe(catchError(this.handleError),
      tap(resData=>{
        // this.handleAuthentication(email, resData.key)
        console.log(resData.message);
      })
    );
  }

  logoutUser(){
    this.user.next(null);
    localStorage.removeItem('userDataDAW');
  }

  autoLogin(){
    const userData: {
      email:string;
      key:string;
      isAuthenticated:boolean;
    } = JSON.parse(localStorage.getItem('userDataDAW'));

    if(!userData){
      return;
    }

    const loadedUser = new User(
      userData.email,
      userData.key,
      userData.isAuthenticated,
    );

    if(loadedUser.token){
      this.user.next(loadedUser);
      // this.autoLogout(14000000);
    }
    this.setLoggedIn(true);
  }

  private handleAuthentication(
    email:string,
    key:string,
  ){
    const user = new User(email, key, true);
    this.user.next(user);
    localStorage.setItem('userDataDAW', JSON.stringify(user));
  }

  private handleError(errorRes:HttpErrorResponse){
    let errorMessage='Por favor verifique sus datos.';
    if(!errorRes.error||!errorRes.error.error){
      return throwError(errorMessage);
    }
    return throwError(errorMessage);
  }



  private handleErrorPassword(errorRes:HttpErrorResponse){
    let errorMessage;
    if(errorRes.error.token){
      errorMessage='Link expirado o inválido.'
    }else if(errorRes.error.new_password2){
      errorMessage=errorRes.error.new_password2;
    }
    return throwError(errorMessage);
  }

}
