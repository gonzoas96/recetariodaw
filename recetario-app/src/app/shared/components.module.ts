
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CategoryCarouselComponent } from './category-carousel/category-carousel';
import { FoodLoaderComponent } from './food-loader/food-loader.component';


import { FooterComponent } from './footer/footer.component';
import { LoadingSpinnerComponent } from './loadingspinner/loadingspinner.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    FooterComponent,
    LoadingSpinnerComponent,
    NavbarComponent,
    FoodLoaderComponent,
    CategoryCarouselComponent
  ],
  exports: [
    FooterComponent,
    LoadingSpinnerComponent,
    NavbarComponent,
    FoodLoaderComponent,
    CategoryCarouselComponent
  ]
})
export class ComponentsModule {}
