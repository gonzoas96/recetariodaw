import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { UpdateService } from 'src/app/services/update.service';

@Component({
  selector: 'app-category-carousel',
  templateUrl: './category-carousel.html',
  styleUrls: ['./category-carousel.css']
})
export class CategoryCarouselComponent implements OnInit {

  @Input() cardCategoryName: string = "";
  isLoading = false;
  error = false;
  slides: any = [[]];
  recipes = [];
  constructor(private renderer: Renderer2, private updateService: UpdateService) { }


  chunk(arr: any, chunkSize: number) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }

  ngOnInit() {
    this.updateService.getCategoryRecipes(this.cardCategoryName).subscribe(
      resData=>{
        this.isLoading = false;
        // console.log(resData);
        this.recipes = resData.recipes;
        // console.log(this.recipes);
        this.slides = this.chunk(this.recipes, 3);
    }, errorMessage=>{
        this.isLoading = false;
        this.error = true;
        // console.log(errorMessage);
        // this.error=errorMessage;
    });

  }

  ngAfterViewInit() {
    const buttons = document.querySelectorAll('.btn-floating');
    buttons.forEach((el: any) => {
      this.renderer.removeClass(el, 'btn-floating');
      this.renderer.addClass(el, 'px-3');
      this.renderer.addClass(el.firstElementChild, 'fa-3x');
    });

  }
}
