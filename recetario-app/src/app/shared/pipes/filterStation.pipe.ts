import { Pipe, PipeTransform } from '@angular/core';
import { Station } from '../models/station.model';

@Pipe({
    name: 'filterStation'
})
export class FilterStationPipe implements PipeTransform {
    transform(items: Station[], filter: string, propName: string): any {
        if (!items || !filter) {
            return items;
        }
        const resultArr = [];
        for (let item of items){
          if(item[propName]== filter){
            resultArr.push(item);
          }
        }
        return resultArr;
    }
}
