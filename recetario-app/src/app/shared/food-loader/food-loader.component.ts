import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';



@Component({
  selector: 'app-food-loader',
  templateUrl:  './food-loader.component.html',
  styleUrls: ['./food-loader.component.scss']
})
export class FoodLoaderComponent{
  private intervalSubscription : Subscription;
}

