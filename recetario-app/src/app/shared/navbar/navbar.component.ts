import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  test: Date = new Date();
  emailUser = "";
  user : boolean = false;
  constructor(private authService : AuthService, private router: Router) {}

  ngOnInit() {
    if(this.authService.user.value){
      this.user = true;
      this.emailUser = this.authService.user.value.email;
    }
  }
  logout(){
    this.authService.logoutUser();
    this.user = false;
    this.emailUser = "";
    this.router.navigate(['/home']);
  }

}
