
export class Recipe2 {
  public title: string;
  public imagePath: string;
  public category: string;
  public ingridients: { name: string, amount: string}[];
  public instructions: string;
  public description: string;
  public creator: string;
  public meal : string
  public _id: string;
  public __v: number;

  constructor(title: string="", imagePath: string="", category: string="", ingridients: { name: string, amount: string}[]=[], instructions: string="" , description: string="", creator: string="",_id: string="",__v: number=0, meal: string = ""){
    this.title = title;
    this.imagePath = imagePath;
    this.category = category;
    this.ingridients = ingridients;
    this.instructions = instructions;
    this.description = description;
    this.creator = creator;
    this.meal = meal;
    this._id = _id;
    this.__v = __v;
  }
}
