export class User{

  constructor(
      public email:string,
      public key:string,
      public isAuthenticated:boolean,
  ){}

  get token(){
    return this.key;
  }

  get userIsAuthenticated(){
    return this.isAuthenticated;
  }
}

