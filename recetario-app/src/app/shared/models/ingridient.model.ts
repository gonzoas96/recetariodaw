
export class Ingridient {
  public name: string
  public amount: string

  constructor(name: string, amount: string){
    this.name = name;
    this.amount = amount;
  }


}
