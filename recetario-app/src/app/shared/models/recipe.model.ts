

export class Recipe {
  public title: string;
  public imagePath: string;
  public category: string;
  public ingridients: { name: string, amount: string}[];
  public instructions: string;
  public description: string;
  public creator: string;
  public meal: string;

  constructor(title: string, imagePath: string, category: string, ingridients: { name: string, amount: string}[], instructions: string , description: string, meal: string = "", creator: string=""){
    this.title = title;
    this.imagePath = imagePath;
    this.category = category;
    this.ingridients = ingridients;
    this.instructions = instructions;
    this.description = description;
    this.creator = creator;
    this.meal = meal;
  }


}
