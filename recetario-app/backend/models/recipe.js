const { strict } = require('assert');
const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
  title: { type: String, required: true},
  imagePath: { type: String, required: true},
  category: { type: String },
  ingridients: [{ name: String , amount : String }],
  instructions: { type: String },
  description: {type: String},
  meal: {type: String},
  creator: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true}
})

module.exports = mongoose.model('Recipe',recipeSchema);
