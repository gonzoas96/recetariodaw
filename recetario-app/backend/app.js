const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var cors = require('cors')
//agrega las http routes de recipes.js
const postsRoutes = require("./routes/recipes");
const userRoutes = require("./routes/user");

app.use("/",express.static(path.join(__dirname, "angular")));
//MongoDB psw: xrWJHlkL8f530uCX
//MongoConnected
mongoose.connect("mongodb+srv://gonzoas:xrWJHlkL8f530uCX@cluster0.bokko.mongodb.net/daw-proyect")
  .then(() => {
    console.log("connected to database");
  })
  .catch(() => {
    console.log("Connection failed");
  })

  app.use(cors());
//CORS HEADERS
// app.use((req,res,next) => {
//   res.setHeader('Access-Control-Allow-Origin',"*");
//   res.setHeader('Access-Control-Allow-Headers',"Origin, X-Requested-With, Accept");
//   res.setHeader('Access-Control-Allow-Methods',"GET, POST, PUT, DELETE, OPTIONS");
//   next();
// });


// JSON MIDDLEWARE
app.use(bodyParser.json());

app.use("/api/recipe", postsRoutes);
app.use("/api/user", userRoutes);
app.use((req,res,next) =>{
  res.sendFile(path.join(__dirname,"angular","index.html"));
});


module.exports = app;
