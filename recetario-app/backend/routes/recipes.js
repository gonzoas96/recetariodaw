const express = require("express");


const Recipe = require('../models/recipe');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

//Ruta para crear una nueva receta con middleware de checkauth integrado
router.post("",checkAuth ,(req,res,next) => {
  const recipe = new Recipe(
     {
      title: req.body.title,
      imagePath: req.body.imagePath,
      category : req.body.category,
      ingridients: req.body.ingridients,
      instructions: req.body.instructions,
      description : req.body.description,
      creator: req.userData.userId,
      meal: req.body.meal
     }
  );
  recipe.save().then(createdRecipe => {
    res.status(201).json({
      message: 'Post added successfully',
      recipeId: createdRecipe._id
    })
  })
  .catch(err => {
    res.status(500).json({
      error: err
    })
  })
});

// adquiere toda las recetas
router.get("",(req,res,next) => {
  Recipe.find().then( (recetas) => {
    res.status(200).json({
      message: 'Recipe fetched successfully',
      recipes : recetas
    });
  });
});

// adquiere toda las recetas de un usuario
router.get("/myRecipes",checkAuth,(req,res,next) => {
  Recipe.find({creator: req.userData.userId }).then( (recetas) => {
    if (recetas){
      res.status(200).json({
        message: 'your recipes fetched successfully',
        recipes: recetas
      });
    }else{
      res.status(401).json({
        message: 'Recipe not found'
      });
    }
  });
});



//Ruta para obtener una sola receta
router.get("/:id",(req,res,next) => {
  Recipe.findById(req.params.id).then( (receta) => {
    if (receta){
      res.status(200).json({
        message: 'Recipe fetched successfully',
        recipe : receta
      });
    }else{
      res.status(401).json({
        message: 'Recipe not found'
      });
    }
  });
});

//Ruta para modificar una sola receta y eso solo se puede al estar autorizado
router.put("/:id" ,checkAuth ,(req,res,next) => {
  const recipe = new Recipe(
    {
      title: req.body.recipe.title,
      imagePath: req.body.recipe.imagePath,
      category: req.body.recipe.category,
      ingridients: req.body.recipe.ingridients,
      instructions: req.body.recipe.instructions,
      description: req.body.recipe.description,
      meal: req.body.recipe.meal,
      _id: req.body.recipe._id,
     creator: req.userData.userId
    }
  );

  Recipe.updateOne({ _id : req.params.id, creator: req.userData.userId}, recipe).then( result =>{
    if(result.nModified > 0){ // checa si efectivamente se modificó
      res.status(200).json({
        message: 'Recipe modified successfully'
      });
    }else{
      res.status(402).json({
        message: 'Not authorized to modify'
      });
    }
  });
});

//ruta para obtener receta random
router.get("/random/:meal",(req,res,next) => {
  const meal= req.params.meal;
  Recipe.aggregate([
    {$match: {meal : meal}}, // filter the results
    {$sample: {size: 1}} // You want to get 1 docs
  ]).then((recipe)=>{
    if (recipe){
      res.status(200).json({
        message: 'Recipe fetched successfully',
        recipe : recipe
      });
    }else{
      res.status(401).json({
        message: 'Recipe not found'
      });
    }
  });
});

//Ruta para obtener recetas de una categoria
router.get("/category/:category",(req,res,next) => {
  Recipe.find({category: req.params.category}).then( (recetas) => {
    if (recetas){
      res.status(200).json({
        message: 'Recipe fetched successfully',
        recipes : recetas
      });
    }else{
      res.status(401).json({
        message: 'Recipe not found'
      });
    }
  });
});


//Ruta para borrar una sola receta
router.delete("/:id" ,checkAuth ,(req,res,next) => {
  Recipe.deleteOne({_id : req.params.id, creator: req.userData.userId }).then( result =>{
    if(result.n > 0){ // checa si efectivamente se eliminó
      res.status(200).json({
        message: 'Recipe deleted successfully'
      });
    }else{
      res.status(402).json({
        message: 'Not authorized to delete'
      });
    }
  });
});


module.exports = router;
